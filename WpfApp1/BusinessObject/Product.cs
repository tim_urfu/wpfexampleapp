﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace WpfExampleApp
{
    public class Product
    {
        public string Name { get; set; }

        public decimal UnitPrice { get; set; }

        public Product(string name, decimal unitPrice)
        {
            Name = name;
            UnitPrice = unitPrice;
        }

        public Product()
        {
        }
    }
}
