﻿using FastReport;
using FastReport.Export.Image;
using FastReport.Export.PdfSimple;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfExampleApp
{
    /// <summary>
    /// Interaction logic for WindowReport.xaml
    /// </summary>
    public partial class WindowReport : Window
    {
        private List<Category> Categories;

        private List<Product> Products;

        private string inFolder = @"..\..\..\in\";

        public WindowReport()
        {
            InitializeComponent();

            inFolder = Utils.FindDirectory("in");
            //outFolder = Directory.GetParent(inFolder).FullName + "\\out";
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateBusinessObject();

            // create report instance
            Report report = new Report();

            // load the existing report
            report.Load($@"{inFolder}\report.frx");

            // register the array
            report.RegisterData(Categories, "Categories");
            report.RegisterData(Products, "Products");

            // prepare the report
            report.Prepare();

            // save prepared report
            //if (!Directory.Exists(outFolder))
            //    Directory.CreateDirectory(outFolder);
            report.SavePrepared("Prepared_Report.fpx");

            // export to image
            ImageExport image = new ImageExport();
            image.ImageFormat = ImageExportFormat.Jpeg;
            report.Export(image, "report.jpg");

            #region -- Export to PDF

            PDFSimpleExport pdfExport = new PDFSimpleExport();

            pdfExport.Export(report, "report.pdf");

            #endregion


            report.Dispose();

            buttonPdf.IsEnabled = true;
            buttonPicture.IsEnabled = true;

            //imagePreview.Source = "report.jpg";
        }

        private void CreateBusinessObject()
        {
            Products = new List<Product>()
            {
                new Product { Name = "Test Value of Product", UnitPrice = 1000 }
            };

            Categories = new List<Category>();

            Category category = new Category("Beverages", "Soft drinks, coffees, teas, beers");
            category.Products.Add(new Product("Chai", 18m));
            category.Products.Add(new Product("Chang", 19m));
            category.Products.Add(new Product("Ipoh coffee", 46m));
            Categories.Add(category);

            category = new Category("Кондитерские изделия", "Desserts, candies, and sweet breads");
            category.Products.Add(new Product("Chocolade", 12.75m));
            category.Products.Add(new Product("Scottish Longbreads", 12.5m));
            category.Products.Add(new Product("Tarte au sucre", 49.3m));
            Categories.Add(category);

            category = new Category("Рыба", "Seaweed and fish");
            category.Products.Add(new Product("Boston Crab Meat", 18.4m));
            category.Products.Add(new Product("Red caviar", 15m));
            Categories.Add(category);
        }

        private void buttonPicture_Click(object sender, RoutedEventArgs e)
        {
            var uriImageSource = new Uri(AppDomain.CurrentDomain.BaseDirectory + @"report.jpg", UriKind.RelativeOrAbsolute);
            imagePreview.Source = new BitmapImage(uriImageSource);
        }

        private void buttonPdf_Click(object sender, RoutedEventArgs e)
        {
            var uri = new Uri(AppDomain.CurrentDomain.BaseDirectory + "report.pdf");
            webViewer.Source = uri;
        }
    }
}
